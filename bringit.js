(function($){
"use strict";	
$.fn.bringIt = function(x, inp1, inp2){
	//check to see if animation is already running, to prevent overlap
	if(window.animationRunning){
		$(document).trigger('animations_start_failed');
		return false;
	}
	//animation start event fires before anything is set 
	$(document).trigger('animations_start');
	//set top level flag to mark animation as running
	window.animationRunning = true;

	//if animating in, dir == true, out == false
	var dir = typeof(x) !== "string" || x !== 'out';
	
	//debug mode switch. Use the string 'debug' as any argument in the function call
	var debug = false;
	$(arguments).each(function(){
		if(this === 'debug'){ debug = true;}	
	});
	
	//containers
	var speed; 
	var link = false;
	
	
	//extract link. Link can be a selector or jQuery object pointing to an anchor tag
	if(x === "out"){
		if(typeof(inp2) === "string"){link = $(inp2);}if(typeof(inp1) === "string"){link = inp1;}
		if(typeof(inp2) === "object"){link = inp2;}if(typeof(inp1) === "object"){link = inp1;}
		if(debug && window.console){ console.log(link)};
	} 
	
	
	
	//log the start position of each element
	$.fn.logPosition = function(){
		
		if($(this).data('startposition')){
			$(this).data('startPos',$(this).data('startposition'));
		}
	
		if(x !== "out"){
			$(this).data($(this).position());
		}
		//keep the start position for reuse
		if(!$(this).data('startPos')){
			$(this).data('startPos',$(this).position());
		}
	};
	//perform movements
	$.fn.movement = function(){
		var posData = $(this).data();
		
		
		
		var direction = (dir ? posData.animin : posData.animout);
		
		
		//ANIMATON HAPPENS HERE
		if(dir){
			$(this).startAnim(direction, posData).animate($(this).firstPositions(), speed, $(this).checkQueue);
		} else {
			$(this).animate(animCoords(posData.animout), speed, $(this).checkQueue);
		}	
	};
	
	//set speed
	$.fn.setSpeed = function(){
		speed = 2000;
			if(typeof(inp1) === "number"){speed = inp1;}if(typeof(x) === "number"){speed = x;}
		if(dir || !$(this).attr('data-speedout')){
			if($(this).attr('data-speed')){ speed = parseInt($(this).attr('data-speed'),10);}
			return this;
		}
		if($(this).attr('data-speedout')){speed = parseInt($(this).attr('data-speedout'),10);}
		
		return this;
	};
	
	//returns an object for populating the animate in function
	$.fn.firstPositions = function(){
		$(this).data('startPos').opacity = 1;
		return $(this).data('startPos');
			
		
	};
	
	//work out the opposite direction
	var opposite = function(x){
		switch (x){
			case 'up': 
				return 'up';//returns up as it wants to go 
			case 'down':
				return 'down';
			case 'left':
				return 'right';
			case 'right':
				return 'left';
			default:
				return 'fade';
		}
	};
	
	//position the elements out side of the window
	$.fn.startAnim = function(direction, posData){
		if(is_direction(direction)){
			$(this).css(animCoords(direction));
		}
		return this;
	};
	
	//switch to create position object for startAnim
	var animCoords = function(direction){
		switch (direction){
			case 'up':
				return ({"top" : $(window).height()*2+"px"});
			case 'down':
				return ({"top" : "-"+$(window).height()+"px"});
			case 'left':
				return ({"left" : "-"+$(window).width()+"px"});
			case 'right':
				return ({"left" : $(window).width()*2+"px"});
			default:
				return ({"opacity" : 0});
		}
		return this;
	};
	
	//works out if a string is a direction, so we can use fade as a smart default
	var is_direction = function(string){
		if (typeof(string) && (string === "up" || string === "down" || string === "left" || string === "right")){
			return true;
		}
		return false;
	};
	
	//ensures an animate out exists for directions by applying opposite direction
	$.fn.addOpposite = function(){
		if(typeof($(this).attr('data-animin')) !== "string" || !is_direction($(this).attr('data-animin'))){
			$(this).data('animout', $(this).attr('data-animout'));
			return this;
		}
		if(typeof($(this).attr('data-animout')) !== 'string'){
			$(this).data('animout', $(this).attr('data-animin'));
		}
		return this;
	};
	
	/*
	# Process
	*/
	//required to establish end of animation queue
	var elements = $(this).length;
	
	//checks complete items against elements in list
	//fire event when all animations are complete
	$.fn.checkQueue = function(){
		elements--;
		if(elements === 0){
			$.event.trigger('animations_complete');
		}
	};
	this.each(function(){
			
			//log position
			$(this).setSpeed().addOpposite().logPosition();
	
			//call animation
			$(this).movement();
		});
		
		
    
	//finish animation, remove flag, go to link if exists
	$(document).bind('animations_complete',function(){
		window.animationRunning = false;
			if(link){ window.location = $(link).attr('href');}
		});
	};
	
})(jQuery);

/*
#
#    ------Events------
#

$(document).bind('animations_start',function(){
	
});
$(document).bind('animations_start_failed',function(){
	//fired when plugin fired whilst already running
});
$(document).bind('animations_complete',function(){

});
*/
